# SteamLR v1.5.0 - 10 august 2018

SteamLR starts Steam in lower-resolution
that to increase game performances
then after your session over, SteamLR restores original desktop resolution

# Usage:

    steam-lr [-W|--WINE] [-b|--big-picture] [-r|--resolution ...] [-g|--game ...] [-f|--fastest] [-q|--quality ...] [-h|--help)]

## Core Options

    -r | --resolution [WxH|list]	specify the resoltion of steam
				1280x720 by default
				use 'list' option to get available resoltions
    -g | --game [gameID|list|menu]	autorun game with its ID
				use 'list' to get available games
				use 'menu' to select game from list
    -b | --big-picture		enable Steam Big Picture mode
    -h | --help			this help

## Unity3D games options
*appliable on top of -g option*

    -f | --fastest			use fastest details (= -q fastest)
    -q | --quality			set quality to [fastest|fast|simple|good|beautiful|fantastic]

## Experimental options

    -W | --WINE			use Steam for Windows under Wine
				(if specified, it has to be set as first parameter)

## Exemples

    steam-lr
    steam-lr -b
    steam-lr -r 1024x768 -g menu -f
    steam-lr -g list
    steam-lr -g menu
    steam-lr -W -g menu
    steam-lr -W -g 448510
